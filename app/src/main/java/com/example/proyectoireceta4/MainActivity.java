package com.example.proyectoireceta4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView Texingrediente;
    private EditText Idbuscar;
    private Button BBboton1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.Texingrediente = (TextView) findViewById(R.id.Texingrediente);
        this.Idbuscar = findViewById(R.id.Idbuscar);
        this.BBboton1 = findViewById(R.id.BBboton1);


        this.BBboton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = Idbuscar.getText().toString().trim();


                String url = "https://api.edamam.com/search?q=" + name + "&app_id=%20$%20a9217e9d%20&%20app_key%20=%20$%20f0082146a81454470315f08c5a45500c%20y%20";

                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,

                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);
                                    String ingredientLines = respuestaJSON.getString("ingredientLines");


                                    // si quieres poner otro valor como farenhit los pones broo aquiii
                                    // double pressure = mainJSON.getDouble("pressure");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );


                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);


            }


        });
    }
}